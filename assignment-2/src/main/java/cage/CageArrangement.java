package cage;

import java.util.Arrays;
import java.util.ArrayList;

public class CageArrangement{

	public static final int nLevel = 3;

	/* function that splitting cage into 3 level */
	public static ArrayList<Cage>[] arrangeCage(Cage[] cages){

		/* Array (perLevel) with ArrayList as element of the Array */
		ArrayList<Cage>[] perLevel = new ArrayList[nLevel];
		for (int i = 0; i < nLevel; i++)
			perLevel[i] = new ArrayList<Cage>();

		/* handle if cages < nLevel */
		if (cages.length < nLevel)
			for (int i = 0; i < cages.length; i++)
				perLevel[i].add(cages[i]);
		
		/* otherwise */
		else {
			int[] nLv = new int[3];
			int n;

			n = cages.length;
			nLv[0] = nLv[1] = n/3;
			nLv[2] = (n/3) + (n%3);

			/* init / fill (arrange) */
			int idx = 0;
			for (int i = 0; i < nLevel; i++){ 		// i: current level
				for (int j = 0; j < nLv[i]; j++){ 	// iterate nLv[i] times to add cage in perLevel[i]   
					perLevel[i].add(cages[idx]); 	// idx: current cage in cages
					idx++;
				}
			}
		}

		return perLevel;
	}

	/* rearrangement -> add 1 level (n+1 mod 3) & reverse */
	public static ArrayList<Cage>[] rearrangementCage(ArrayList<Cage>[] perLevel){
		ArrayList<Cage>[] newPerLevel = new ArrayList[nLevel];

		/* fill newPerLevel with reversed element in perLevel */
		for (int i = 0; i < nLevel; i++){

			/* init */
			newPerLevel[i] = new ArrayList<Cage>();

			for (int j = perLevel[i].size() - 1; j >= 0 ; j--){
				 newPerLevel[i].add(perLevel[i].get(j));
			}
		}

		/* add 1 level */
		ArrayList<Cage> temp = newPerLevel[2];
		newPerLevel[2] = newPerLevel[1];
		newPerLevel[1] = newPerLevel[0];
		newPerLevel[0] = temp;

		return newPerLevel;
	}

	public static void printt(ArrayList<Cage>[] level){
		for (int i = nLevel - 1 ; i >= 0; i--){
			System.out.printf("Level %d:", i + 1);
			for (int j = 0; j < level[i].size(); j++)
				System.out.printf(" %s,", level[i].get(j).getData());
			System.out.println();
		}
		System.out.println();
	}
}