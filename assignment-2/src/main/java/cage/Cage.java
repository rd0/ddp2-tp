package cage;

import animal.Animal;
import animal.Cat;
import animal.Eagle;
import animal.Hamster;
import animal.Lion;
import animal.Parrot;

public class Cage{
	
	Cat cat;
    Hamster hamster;
    Parrot parrot;
    Eagle eagle;
    Lion lion;
	private Animal animal;

	private boolean isIndoor; // 1 = Indoor, 0 otherwise
	private String cageType; // {A, B, C}
	private String cageData;

	public Cage(Animal animal){
		this.animal = animal;
		this.isIndoor = !animal.getIsWild();
		if ((!isIndoor && animal.getLength() < 75) || (isIndoor && animal.getLength() < 45))
			this.cageType = "A";
		else if ((!isIndoor && animal.getLength() <= 90) || (isIndoor && animal.getLength() <= 60))
			this.cageType = "B";
		else
			this.cageType = "C";
		this.cageData = String.format("%s (%d - %s)", animal.getName(), animal.getLength(), cageType);
	}

	/* set & get method */

	public Animal getAnimal(){
		return this.animal;
	}

    public String getLoc() {
        if (this.isIndoor) {
            return "indoor";
        } else {
            return "outdoor";
        }
    }

	public String getType(){
		return this.cageType;
	}

	public String getData(){
		return this.cageData;
	}

}