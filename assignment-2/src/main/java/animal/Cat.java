package animal;

import java.util.Random;

public class Cat extends Animal{

	private String[] sounds = {"Miaaaw..", "Purrr..", "Mwaw!", "Mraaawr!"};

	public Cat(String name, int length){
		super(name, length, false);
	}

	public String fur(){
		return "Nyaaan...";
	}

	public String cuddling(){
		return sounds[new Random().nextInt(4)];
	}

}