package animal;

public class Parrot extends Animal{ 

	public Parrot(String name, int length){
		super(name, length, false);
	}

	public String fly(){
		return "TERBAAAANG….";
	}

	public String repeat(String words){
		return words.toUpperCase();
	}
}