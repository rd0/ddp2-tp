import animal.Cat;
import animal.Eagle;
import animal.Hamster;
import animal.Lion;
import animal.Parrot;


public class CareTaker{
	
    public static void askName(String type) {
        System.out.printf("Mention the name of %s you want to visit: ", type);
    }

    public static void notFound(String type) {
        System.out.printf("There is no %s with that name! ", type);
    }

    public static void visiting(String name, String type) {
        System.out.printf("You are visiting %s (%s) now, what would you like to do?%n", name, type);
    }

    public static void doNothing(){
        System.out.println("You do nothing...");
    }

    public static void back2office(){
        System.out.println("Back to the office!");
    }

    public static void makeVoice(String name, String voice){
        System.out.printf("%s makes a voice: %s%n", name, voice);
    }

    public static void furCat(Cat cat){
        System.out.printf("Time to clean %s's fur", cat.getName());
        makeVoice(cat.getName(), cat.fur());
    }

    public static void cuddlingCat(Cat cat){
        makeVoice(cat.getName(), cat.cuddling());
    }

    public static void flyEagle(Eagle eagle){
        makeVoice(eagle.getName(), eagle.fly());
        System.out.println("You hurt!");
    }

    public static void flyParrot(Parrot parrot){
        makeVoice(parrot.getName(), parrot.fly());
    }

    public static void gnawHamster(Hamster hamster){
        makeVoice(hamster.getName(), hamster.gnaw());
    }

    public static void onWheelHamster(Hamster hamster){
        makeVoice(hamster.getName(), hamster.onWheel());
    }

    public static void repeatParrot(Parrot parrot, String words){
        System.out.printf("%s says: %s%n", parrot.getName(), parrot.repeat(words));
    }

    public static void huntLion(Lion lion) {
        System.out.println("Lion is hunting..");
        makeVoice(lion.getName(), lion.hunt());
    }

    public static void brushLionMane(Lion lion) {
        System.out.printf("Clean the lion's mane..");
        makeVoice(lion.getName(), lion.scream());
    }
}