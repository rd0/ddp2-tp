package card;

import javax.swing.JButton;
import javax.swing.ImageIcon;

import java.awt.Image;

/**
 * Class of Card will be use in board game
 *
 * @author Yusuf T Ardho
 */

public class Card {

	private final static String path = "/home/rdo/ddp2-tp/assignment-4/src/img/";
	private final ImageIcon imgBack, img;
	private final JButton button;
	private final int id;
	private boolean isMatched;
	private boolean faceDown;

	/**
	 * Constructor of Card
	 * @param id id of the card
	 * @param img image of the card
	 */

	public Card(int id, ImageIcon img) {
		this.id = id;
		this.faceDown = true;
		this.isMatched = false;
		this.button = new JButton();
		this.imgBack = setImgSize(new ImageIcon(path + "back.png"));
		this.img = setImgSize(img);
		this.button.setIcon(imgBack);
	}

	/**
	 * Accessor to get id of a card
	 * @return id of the card
	 */

	public int getId() {
		return this.id;
	}

	/**
	 * Accessor to get is matched / otherwise
	 * @return true if matched, false otherwise
	 */

	public boolean getIsMatched() {
		return this.isMatched;
	}

	/**
	 * Accessor to get button of the card
	 * @return button
	 */

	public JButton getButton() {
		return this.button;
	}

	/**
	 * Mutator to set isMatched
	 * @param matched that will be assign to isMatched 
	 */

	public void setMatched(boolean matched) {
		this.isMatched = matched;
	}

	/**
	 * Method to face down the card
	 */

	public void faceDown() {
		button.setIcon(imgBack);
	}

	/**
	 * Method to face up the card
	 */

	public void faceUp() {
		button.setIcon(img);
	}

    /**
     * Method to set image icon size
     * @param imageIcon image that want to be resized
     * @return resized image 
     */

    public static ImageIcon setImgSize(ImageIcon imageIcon) {
        Image image = imageIcon.getImage();
        Image resizedImage = image.getScaledInstance(100, 100, java.awt.Image.SCALE_SMOOTH);
        return new ImageIcon(resizedImage);
    }

}