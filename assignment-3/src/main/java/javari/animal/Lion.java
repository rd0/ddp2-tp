package javari.animal;

/**
 * This class represents specific attributes and behaviours found in lions
 * in Javari Park.
 *
 * @author Yusuf T Ardho
 */
public class Lion extends Mammals {

    /**
     * Constructs an instance of {@code Lion}.
     */
    public Lion(Integer id, String type, String name, Gender gender, double length,
                double weight, Condition condition, String specificCondition) {
        super(id, type, name, gender, length, weight, condition, specificCondition);
    }

    /**
     * to know whether an animal is able to perform or not.
     *
     * @return
     */
    protected boolean specificCondition(){
        return super.specificCondition() && (this.getGender() == Gender.MALE);
    }
}