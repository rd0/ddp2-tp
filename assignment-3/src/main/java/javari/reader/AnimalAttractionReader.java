package javari.reader;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

/**
 * This class represents the base class for reading all lines from a text
 * file that contains CSV data.
 *
 * @author Yusuf T Ardho
 */
public class AnimalAttractionReader extends CsvReader {

    /**
     * Defines the base constructor for instantiating an object of
     * {@code CsvReader}.
     *
     * @param file  path object referring to a CSV file
     * @throws IOException if given file is not present or cannot be read
     *     properly
     */
    public AnimalAttractionReader(Path file) throws IOException {
        super (file);
    }

    /**
     * Counts the number of valid records from CSV file.
     *
     * @return
     */
    public long countValidRecords() {
        List<String> strList = this.getLines();
        String[] cofList = {"Whale", "Lion", "Eagle"};
        String[] daList = {"Parrot", "Snake", "Cat", "Hamster"};
        String[] cmList = {"Parrot", "Whale", "Hamster"};
        String[] pcList = {"Snake", "Cat", "Hamster"};

        long ans = 0;
        int flag_COF, flag_DA, flag_CM, flag_PC;

        flag_COF = 1; flag_DA = 1; flag_CM = 1; flag_PC = 1;

        for (String item : strList) {
            String[] current = item.split(",");
            if (current[1].equalsIgnoreCase("Circles of Fires")) {
                boolean sem = false;
                for (int i = 0; i < 3; i++)
                    if (current[0].equalsIgnoreCase(cofList[i])) 
                        sem = true;
                if (!sem) 
                    flag_COF = 0;
            }
            if (current[1].equalsIgnoreCase("Dancing Animals")) {
                boolean sem = false;
                for (int i = 0; i < 4; i++)
                    if (current[0].equalsIgnoreCase(daList[i])) 
                        sem = true;
                if (!sem) 
                    flag_DA = 0;
            }
            if (current[1].equalsIgnoreCase("Counting Masters")) {
                boolean sem = false;
                for (int i = 0; i < 3; i++)
                    if (current[0].equalsIgnoreCase(cmList[i])) 
                        sem = true;
                if (!sem) 
                    flag_CM = 0;
            }
            if (current[1].equalsIgnoreCase("Passionate Coders")) {
                boolean sem = false;
                for (int i = 0; i < 3; i++)
                    if (current[0].equalsIgnoreCase(pcList[i]))
                        sem = true;
                if (!sem) 
                    flag_PC = 0;
            }
        }
        
        return flag_COF + flag_DA + flag_CM + flag_PC;
    }

    /**
     * Counts the number of invalid records from CSV file.
     *
     * @return
     */
    public long countInvalidRecords() {
        long ans = 4 - countValidRecords();
        List<String> strList = this.getLines();
        for (String item : strList) {
            String[] current = item.split(",");
            if (!current[1].equalsIgnoreCase("Circles of Fires") &&
                !current[1].equalsIgnoreCase("Dancing Animals") &&
                !current[1].equalsIgnoreCase("Counting Masters") &&
                !current[1].equalsIgnoreCase("Passionate Coders")) 
                    ans++;
        }

        return ans;
    }
}
