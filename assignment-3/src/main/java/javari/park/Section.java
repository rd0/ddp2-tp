package javari.park;

import java.util.ArrayList;
import javari.animal.Animal;

/**
 * All the operation in Section (abstract) class
 *
 * @author Yusuf T Ardho
 */
public abstract class Section {

    protected String name;
    protected ArrayList<Animal> animalList;

    /**
     * Construct the sections in Javari park
     *
     * @param name of the section
     */
    public Section(String name) {
        this.name = name;
        this.animalList = new ArrayList<Animal>();
    }

    /**
     * display Attraction menu
     *
     * @return true if made a new record
     */
    public abstract Attraction menu();

    /**
     * Method to add animal to the section
     *
     * @param animal
     */
    public void addAnimal(Animal animal) {
        animalList.add(animal);
    }

}