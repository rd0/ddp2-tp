package javari;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import javari.animal.Animal;
import javari.park.Attraction;
import javari.park.ExploreTheMammals;
import javari.park.ReptillianKingdom;
import javari.park.RegistSys;
import javari.park.Section;
import javari.park.SelectedAttraction;
import javari.park.WorldOfAves;
import javari.reader.AnimalAttractionReader;
import javari.reader.AnimalCategoriesReader;
import javari.reader.AnimalRecordsReader;
import javari.reader.CsvReader;
import javari.writer.RegistrationWriter;

/**
 * Javari Park Class is the main method for the assignment 3
 *
 * @author Yusuf T Ardho
 */

public class A3Festival {

    public static void main(String[] args) {

        Section etm_sec = new ExploreTheMammals();
        Section woa_sec = new WorldOfAves();
        Section rk_sec = new ReptillianKingdom();
        RegistSys registration = new RegistSys(1, "");

        Scanner scan = new Scanner(System.in);

        System.out.println("Welcome to Javari Park Festival - Registration Service!\n");
        System.out.print("... Opening default section database from data. ");
        System.out.println("... File not found or incorrect file!\n");
        System.out.print("Please provide the source data path: ");
        String cmd = scan.nextLine();

        Path startPath = Paths.get(cmd);
        Path categoriesPath = Paths.get(cmd + "\\animals_categories.csv");
        Path attractionsPath = Paths.get(cmd + "\\animals_attractions.csv");
        Path recordsPath = Paths.get(cmd + "\\animals_records.csv");

        AnimalAttractionReader attractionReader;
        AnimalCategoriesReader categoriesReader;
        AnimalRecordsReader recordsReader;
        ArrayList<Animal> animalList = new ArrayList<Animal>();

        try {
            categoriesReader = new AnimalCategoriesReader(categoriesPath);
            attractionReader = new AnimalAttractionReader(attractionsPath);
            recordsReader = new AnimalRecordsReader(recordsPath);

            System.out.println("\n... Loading... Success... System is populating data...\n");
            System.out.printf("Found %d valid sections and %d invalid sections\n",
                    categoriesReader.countValidSections(), categoriesReader.countInvalidSections());
            System.out.printf("Found %d valid attractions and %d invalid attractions\n",
                    attractionReader.countValidRecords(), attractionReader.countInvalidRecords());
            System.out.printf("Found %d valid animal categories and %d invalid animal categories\n",
                    categoriesReader.countValidRecords(), categoriesReader.countInvalidRecords());
            System.out.printf("Found %d valid animal records and %d invalid animal records\n\n",
                    recordsReader.countValidRecords(), recordsReader.countInvalidRecords());

            animalList = recordsReader.getList();
        } 
        catch(IOException e) {
            System.out.println("Error, exitting system...");
            System.exit(0);
        }

        for (Animal current_animal : animalList) {
            if (current_animal.getType().equalsIgnoreCase("Snake"))
                rk_sec.addAnimal(current_animal);
            else if (current_animal.getType().equalsIgnoreCase("Eagle") ||
                    current_animal.getType().equalsIgnoreCase("Parrot"))
                woa_sec.addAnimal(current_animal);
            else
                etm_sec.addAnimal(current_animal);
        }

        System.out.println("Welcome to Javari Park Festival - Registration Service!\n");
        System.out.print("Please answer the questions by typing the number. ");
        System.out.println("Type # if you want to return to the previous menu\n");

        while (true) {
            System.out.println("Javari park has 3 sections:");
            System.out.println("1. Explore the Mammals");
            System.out.println("2. World of Aves");
            System.out.println("3. Reptilian Kingdom");
            System.out.print("Please choose your preferred section (type the number): ");
            cmd = scan.nextLine();

            Attraction att;

            if (cmd.equals("#")) 
                break;

            int pick = Integer.parseInt(cmd);

            if (pick == 1)
                att = etm_sec.menu();
            else if (pick == 2)
                att = woa_sec.menu();
            else //if (pick == 3)
                att = rk_sec.menu();

            if (att != null) {
                System.out.println("Wow, one more step,");
                System.out.print("please let us know your name :");
                String visitor = scan.nextLine();

                System.out.println("\nYeay, final check!");
                System.out.println("Here is your data and the attraction you choose:");
                System.out.printf("Name : %s\n", visitor);
                System.out.printf("Attractions: %s -> %s\n", att.getName(), att.getType());

                System.out.print("With:");
                for (Animal item : att.getPerformers()) 
                    System.out.printf(" %s,",item.getName());

                System.out.print("\n\nIs the data correct? (Y/N): ");
                cmd = scan.nextLine();

                if (cmd.equalsIgnoreCase("Y")) {
                    registration.setVisitorName(visitor);
                    registration.addSelectedAttraction(att);
                    System.out.print("Thank you for your interest. " +
                            "Would you like to register to other attractions? (Y/N): ");
                    cmd = scan.nextLine();

                    if (cmd.equalsIgnoreCase("N")) {
                        try {
                            RegistrationWriter.writeJson(registration, startPath);
                        }
                        catch (IOException e){
                            System.out.println("There is something wrong...");
                            System.exit(0);
                        }

                        System.exit(0);
                    }
                }
            }
        }

    }
}
